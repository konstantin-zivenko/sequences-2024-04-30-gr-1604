# Завдання 2
# Є два списки, які наповнюються користувачем з клавіатури.
# Сформувати список, в якому будуть міститися унікальні значення першого відносно другого списку
# та навпаки без повторень. Роздрукувати підсумковий об'єкт на екран в прямій послідовності,
# зворотній, а також виконати сортування за зростанням та спаданням.


numbers = [
    int(input("Please type the numbers for the first list: ")) for _ in range(10)
]
row_numbers_first, row_numbers_second = numbers[:5], numbers[5:]

unique_values_first = [
    item for item in row_numbers_first if item not in row_numbers_second
]

print(
    f"base list: {unique_values_first}"
    f"\nreverse list: {unique_values_first[::-1]}"
    f"\nascending list: {sorted(unique_values_first)}"
    f"\ndescending list: {sorted(unique_values_first, reverse=True)}"
)
